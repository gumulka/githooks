#!/bin/bash

for hook in applypatch-msg pre-applypatch post-applypatch pre-commit pre-merge-commit prepare-commit-msg commit-msg post-commit pre-rebase post-checkout post-merge pre-push pre-receive update post-receive post-update push-to-checkout pre-auto-gc post-rewrite sendemail-validate fsmonitor-watchman p4-pre-submit post-index-change
do
    if [ ! -e ${hook} ]; then
        echo "#!/bin/bash" > ${hook}
        echo "" >> ${hook}
        echo "exitcode=0" >> ${hook}
        echo "" >> ${hook}
        echo "if [ -x \${PWD}/.git/hooks/${hook} ]; then" >> ${hook}
        echo "    \$PWD/.git/hooks/${hook} \$@ || exitcode=\$?" >> ${hook}
        echo "fi" >> ${hook}
        echo "" >> ${hook}
        echo "# Please insert your checks here." >> ${hook}
        echo "" >> ${hook}
        echo "exit \${exitcode}" >> ${hook}
        chmod a+x ${hook}
    fi
done

# My git hooks

This is a collection of my global git hooks.

For more information on the specific workings visit:

[https://gumulka.com/blog/git-hooks.html](https://gumulka.com/blog/git-hooks.html)

## Getting started

Copy this repo to ~/.githooks/

add the following to ~/.gitconfig :

```ini
[core]
    hooksPath = ~/.githooks/
```

and finally execute create_skripts.sh to create all remaining scripts.
